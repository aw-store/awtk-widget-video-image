﻿#!/usr/bin/python

import os
import sys
import platform

# 序列帧的文件名格式
video_image_format="s%02d"

# 视频文件保存的颜色格式，需要对应 LCD 的类型
video_image_bitmap_format="RGBA8888"

# 序列帧的帧间延迟时间（每一帧的时间间隔，单位为毫秒）
video_image_delay=80

# 由于部分板子的贴图需要对齐，所以这里是需要设置 line_length 对齐模式，具体的请看文档
# video_image_line_length_model="0:16"

# 将 gif 文件拆解成序列帧后保存相关信息的文件名，与 gif_to_frame_gen 工具生成的信息文件名一致
gif_info_file_name="info.log"

def getAwtkRoot(ctx):
  if 'awtk_root' in ctx:
    return ctx['awtk_root']
  else:
    import awtk_locator as locator
    locator.init()
    return locator.AWTK_ROOT

# 根据平台来自定选择 LCD 的类型，但是实际情况有可能不一样，所以用户可以自行修改下面的函数。
def updateVideoImageBitmapFormat(awtk_root):
  global video_image_bitmap_format

  sys.path.insert(0, awtk_root)
  import awtk_config as awtk
  # 这里的判读是为了兼容 awtk 的默认 LCD 类型而设计的，所以用户如果实际情况不一样，请手动修改。
  if os.environ['NANOVG_BACKEND'] == 'AGGE' or os.environ['NANOVG_BACKEND'] == 'AGG' or os.environ['VGCANVAS'] == 'CAIRO':
    video_image_bitmap_format="BGR565"

  print("video_image_bitmap_format:", video_image_bitmap_format);

def joinPath(root, subdir):
  return os.path.normpath(os.path.join(root, subdir))

def remove_dirs(path):
    if not os.path.exists(path):
        return
    ls = os.listdir(path)
    for iter in ls:
        r_path = os.path.join(path, iter)
        if os.path.isdir(r_path):
            remove_dirs(r_path)
        else:
            os.remove(r_path)
    os.rmdir(path)

def genGifFrameImage(tool_path, gif_file, frame_dir):
  if os.path.exists(tool_path):
    cmd = tool_path + " " + os.path.abspath(gif_file) + " " + os.path.abspath(frame_dir)
    os.system(cmd)
  else :
    print(" not find gif_to_frame_gen")

def genOneVideoImage(tool_path, image_dir, image_name_format, save_file_path, frame_image_type, delays):
  if os.path.exists(tool_path):
    cmd = tool_path + " " + os.path.abspath(image_dir) + " " + image_name_format + " " + os.path.abspath(save_file_path) + " " + frame_image_type + " " +delays
    if "video_image_line_length_model" in globals().keys() :
      cmd += (" " + video_image_line_length_model)
    print(cmd)
    os.system(cmd)
  else :
    print(" not find diff_image_to_video_gen")

def genVideoImageGen(src_dir):
  global video_image_line_length_model
  data_dir = joinPath(src_dir, 'data')
  if os.path.exists(data_dir):
    video_dir = joinPath(src_dir, 'images/video')
    if os.path.exists(video_dir):
      gif_tool_path = os.path.abspath('bin/gif_to_frame_gen')
      diff_tool_path = os.path.abspath('bin/diff_image_to_video_gen')
      if platform.system() == "Windows" :
        gif_tool_path += ".exe"
        diff_tool_path += ".exe"
      # 将 gif 图片拆解成序列帧
      if os.path.exists(gif_tool_path):
        for root, dirs, files in os.walk(video_dir):
          for iter in files:
            iter_split = os.path.splitext(iter)
            if root == video_dir and iter_split[-1] == ".gif":
              gif_name = joinPath(root, iter)
              frame_dir = joinPath(root, iter_split[-2])
              genGifFrameImage(gif_tool_path, gif_name, frame_dir)
      # 将序列帧压缩成视频文件
      if os.path.exists(diff_tool_path):
        for root, dirs, files in os.walk(video_dir):
          for iter in dirs:
            if root == video_dir:
              is_gif_image = False
              image_dir = joinPath(root, iter)
              info_file = joinPath(image_dir, gif_info_file_name)
              image_name_format = video_image_format
              save_file_path = joinPath(data_dir, iter)
              frame_image_type = video_image_bitmap_format
              delays = str(video_image_delay)
              if os.path.exists(info_file):
                is_gif_image = True
                with open(info_file, "r", encoding="utf-8") as info:
                  for line in info:
                    line_split = line.split('=')
                    if line_split[0] == "image_name_format":
                      image_name_format = line_split[1].strip('\n')
                    if line_split[0] == "delays":
                      delays = line_split[1].strip('\n')
              genOneVideoImage(diff_tool_path, image_dir, image_name_format, save_file_path, frame_image_type, delays)
              # 删除中间文件
              if is_gif_image:
                remove_dirs(image_dir)

def on_generate_res_before(ctx):
    print('======================= '+ ctx['type'] + ' =======================')
    # ctx = {'type': 'ui', 'theme': 'default', 'imagegen_options': 'mono', 'input': './', 'output': './'}
    if ctx['type'] == "data" :
        updateVideoImageBitmapFormat(getAwtkRoot(ctx))
        genVideoImageGen("design/default")

def on_generate_res_after(ctx):
    print('======================= '+ ctx['type'] + ' =======================')
