﻿#if defined(LINUX) || defined(WIN32)
#include "tkc/fs.h"
#include "tkc/mem.h"
#include "tkc/path.h"
#include "tkc/utils.h"
#include "base/bitmap.h"
#include "base/types_def.h"
#include "image_loader/image_loader_stb.h"
#include <iostream>
using namespace std;

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../3rd/stb/stb_image_write.h"

#define GIF_INFO_FILE_NAME "info.log"
#define FRAME_FILE_NAME_FORMAT "frame%d"
#define FRAME_FILE_NAME_MAX_LEN 16

/**
 * 注意：本工具默认将 GIF 图片解析为RGBA8888格式的位图序列帧，再将它们保存为 png 格式的文件 
 */

static bool get_bitmap(const char* image_name, bitmap_t* image) {
  ret_t ret = RET_FAIL;
  uint8_t* buff = NULL;
  uint32_t buff_length = 0;

  if (!file_exist(image_name)) {
    return false;
  }

  buff = (uint8_t*)file_read(image_name, &buff_length);
  if (buff != NULL) {
    ret = stb_load_image(ASSET_TYPE_IMAGE_GIF, buff, buff_length, image, false, false, false,
                         LCD_ORIENTATION_0);
    TKMEM_FREE(buff);
  }

  return ret == RET_OK;
}

static bool save_git_frame(bitmap_t* image, const char* save_file_path) {
  fs_t* fs = os_fs();
  ret_t ret = RET_OK;
  int x = image->w;
  int y = image->gif_frame_h;
  int stride_bytes = image->line_length;
  char file_path[MAX_PATH];
  char file_name[FRAME_FILE_NAME_MAX_LEN];

  if (!fs_dir_exist(fs, save_file_path)) {
    ret = fs_create_dir_r(fs, save_file_path);
  }

  if (ret == RET_OK) {
    uint8_t* image_data = bitmap_lock_buffer_for_read(image);
    for (int i = 0; i < image->gif_frames_nr; i++) {
      int32_t len = 0;
      uint8_t* png_data = NULL;
      uint8_t* bmp_data = image_data + (stride_bytes * y * i);
      memset(file_path, 0x00, sizeof(file_path));
      memset(file_name, 0x00, sizeof(file_name));

      tk_snprintf(file_name, sizeof(file_name), FRAME_FILE_NAME_FORMAT ".png", i);
      path_build(file_path, sizeof(file_path), save_file_path, file_name, NULL);

      png_data = stbi_write_png_to_mem(bmp_data, stride_bytes, x, y, 4, &len);
      if (png_data != NULL) {
        file_write(file_path, png_data, len);
        STBIW_FREE(png_data);
      }
    }
    bitmap_unlock_buffer(image);
  }

  return ret == RET_OK;
}

int main(int argc, char** argv) {
  bitmap_t image;
  const char* image_name = NULL;
  const char* save_file_path = NULL;
  memset(&image, 0x00, sizeof(bitmap_t));

  if (argc < 3) {
    cout << "argvs: image_name, save_file_path" << endl;
    return -1;
  }

  image_name = argv[1];
  save_file_path = argv[2];

  if (!get_bitmap(image_name, &image)) {
    cout << "get bitmap failed!" << endl;
    return -1;
  }

  if (image.is_gif) {
    if (save_git_frame(&image, save_file_path)) {
      char buff[MAX_PATH] = {0};
      char info_path[MAX_PATH] = {0};
      tk_snprintf(buff, sizeof(buff), "image_name_format=%s\ndelays=%d\n", FRAME_FILE_NAME_FORMAT,
                  *image.gif_delays);
      path_build(info_path, sizeof(info_path), save_file_path, GIF_INFO_FILE_NAME, NULL);
      file_write(info_path, buff, tk_strlen(buff));
    }
  } else {
    cout << "image is not a gif!" << endl;
  }

  bitmap_destroy(&image);

  return 0;
}

#endif
